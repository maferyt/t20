# Partir à l'aventure

Lorsqu'on part à l'aventure, il y a quelques règles à savoir. En voici les principales, les autres seront expliquées par votre *Maître•sse du Jeu* dans le cas échéant.

## Test (jet) de compétence

Lorsque vous souhaitez faire une action, comme tenter d'escalader une montagne, ou crocheter une serrure, le résultat de cette action peut être incertain. Qui n'a jamais trébuché ou cassé ses outils de voleurs ?

Pour savoir si vous savez faire une action, vous allez faire un test de compétence qui sera : `1d20 + bonus de compétence + bonus de caractéristique`. Le• *Maître•sse du Jeu* vous dira ensuite si vous réussissez cette action ou non, ainsi que les conséquences de cette action.

## Test de résistance (Jet de sauvegarde)

Lorsqu'un événement fâcheux arrive, le• *Maître•sse du Jeu* peut vous demander un "test de résistance" (aussi appelé "Jet de sauvegarde"). Il représente votre tentative à résister à un sort, un piège ou toute autre menace similaire.

Ces tests sont simples :

- `1d20 + Bonus de FOR` pour réussir un test de vigueur
- `1d20 + Bonus de DEX` pour tester vos réflexes
- `1d20 + Bonus d'ESP` pour se prémunir de la magie

## Combats

Il arrive, quelques fois, qu'on se retrouve face à une bande de gobelins, à une horde de zombies, ou même à un dragon. Dans ce cas là, il vaut mieux [savoir comment se passe un combat](./combat.md). Cette page dédiée vous dira tout sur les règles à connaître lors d'un affrontement.

## Repos et soins

Dans une aventure, on prend souvent des coups et on utilise (quand on sait en faire) des sorts. Et, comme nous sommes tous des êtres vivants, nous avons besoin de nous reposer, même les héros•ïnes.

La perte de points de vie et de magie peut être soignée en se reposant, et aussi avec des potions. Après une bonne nuit de sommeil, un personnage regagne `l'ensemble de ses points de vie et de magie`.

## Héroïsme

Dans une aventure, les héros•ïnes peuvent parfois faire des actions héroïques. Imaginez que la prochaine action que vous souhaitez faire va faire basculer votre vie, vous serez alors plus concentré•e que vous ne l'avez été dans le reste de la journée. C'est à ce moment là qu'intervient l'héroïsme.

Dans `t20`, vous pouvez décider d'ajouter un dé d'héroïsme au prochain jet que vous allez faire. Votre personnage commence avec `1d6` au niveau 1 et en gagne un supplémentaire à tous les niveaux impairs. 

Vous pouvez le dépenser d'une des façons suivantes :

- Améliorer un jet d’attaque, de compétence ou de sauvegarde
- Améliorer les dégâts d’une attaque
- Réduire les dégâts d’une attaque
- Augmenter la difficulté de résistance à un sort
- Effectuer une action extraordinaire, comme attaquer plusieurs cibles d’un seul coup

Vos dés reviennent naturellement après une bonne nuit de sommeil. Au niveau 10, un personnage peut dépenser deux dés à la fois, et au niveau 20, il peut en dépenser jusqu'à trois à la fois.

## Montée de niveau - Après le niveau 1

Dans `t20`, il n'y a pas de système d'expérience à proprement parler. Le *Maître du Jeu* comptabilise de son côté l'expérience du groupe et tout le monde gagne un niveau en même temps. Chaque niveau supplémentaire vous apporte son lot de puissance, mais aussi de surprises.

Voilà les bonus que vous avez à chaque niveau :

- `5 + Bonus de FOR` points de vie supplémentaires
- `2 + Bonus d'ESP` points de magie supplémentaires
- `+1` à tous les jets d'attaque

Ensuite, en fonction du niveau :

- Si le niveau est *pair*, vous ajoutez `1 point` à une de vos caractéristiques
- Si le niveau est *impair*, vous ajoutez `1 point` à chacune de vos compétences et vous gagnez un dé d'héroïsme

## Jeu de rôle

Le jeu de rôle (ou *Roleplay* - *RP*) est, littéralement, l'action de jouer un rôle. Dans ce cas, c'est vous, en tant que joueur•euse, qui allez déterminer comme votre personnage pense, agit et parle. 

> Le jeu de rôle est partie intégrante de tous les aspects du jeu, et il est mis en avant lors des interactions sociales. L’excentricité, les manières et la personnalité de votre personnage influencent le dénouement de vos interactions.\*

\* Citation [AideDD](https://www.aidedd.org/regles/aventure/#interactions)

Dans `t20`, le•a *Maître•sse du Jeu* peut vous accorder un bonus (de `+1` ou `+2`) à un test si votre jeu de rôle est très bon. C'est totalement subjectif, mais ça vous insitera à mieux entrer dans la peau de votre personnage.

D'ailleurs, si vous souhaitez en savoir plus, n'hésitez pas à aller consulter la [partie "Interactions" sur AideDD](https://www.aidedd.org/regles/aventure/#interactions), elle est très bien écrite.

*À noter* : lorsque vous parlez en dehors de votre personnage, on utilise l'expression "méta" (ou "métagame").
