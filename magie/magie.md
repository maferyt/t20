# Magie

## Lancement du sort

Pour lancer un sort, il faut réunir les conditions suivantes :

- avoir le niveau requis
- avoir les points de magie nécessaires
- réussir un jet de lancement de sort

Le niveau du sort doit être inférieur ou égal à `la moitié du niveau du lanceur de sort, arrondi au supérieur`.

## Réussite du lancement du sort

Ce n'est pas parce qu'on veut lancer un sort, qu'on y arrive. Pour réussir à lancer un sort, il faut réussir un jet de `1d20`. Celui-ci doit être supérieur à `10 - bonus d'ESP du lanceur`.

## Coût de lancement du sort

Lancer un sort, quel qu’il soit, coûte des Points de Magie (ou PM). Le coût correspond au `niveau du sort à lancer`.

Cette perte de points de magie peut être récupérée par des potions ou après une bonne nuit de sommeil. 

**À noter** : il n’est pas nécessaire de mémoriser les sorts à l’avance.

### Puiser dans ses forces

Dans le cas où une personne souhaite lancer des sorts, mais qu'elle n'a plus de *points de magie* disponibles, elle peut puiser dans ses *points de vie*. Dès lors, elle perd un nombre de PV égal à `1 + (niveau du sort * 2)`.

## Extension de la magie

Voici trois moyens pour ajouter un peu de variété au répertoire des lanceurs de sort. Chacun modifie un sort tout en doublant son temps de lancement ; ainsi, un sort qui, non modifié, nécessitait une action simple pour être lancé demande désormais un round entier.

### Extension de durée

Double la durée d’un sort par rapport à la normale. Le sort coûte `2 PM` supplémentaires.

### Extension d’effet

Le sort fait 50% plus de dégâts que la normale et coûte `2 PM` supplémentaires.

### Extension de zone d’effet

La zone d’effet du sort est doublée par rapport à la normale. Le sort coûte `6 PM` supplémentaires.

## Tableau récapitulatif

| Niveau de sort | Niveau requis | Coût en PM | Coût en PV |
|----------------|---------------|------------|------------|
| 0              | -             | 0          | 1          |
| 1              | 1             | 1          | 3          |
| 2              | 3             | 2          | 5          |
| 3              | 5             | 3          | 7          |
| 4              | 7             | 4          | 9          |
| 5              | 9             | 5          | 11         |
| 6              | 11            | 6          | 13         |
| 7              | 13            | 7          | 15         |
| 8              | 15            | 8          | 17         |
| 9              | 17            | 9          | 19         |

## Magies disponibles

Il existe 4 types de magie disponible :

- [Magie divine](./divine.md)
- [Magie druidique](./druidique.md)
- [Magie des illusions](./illusion.md)
- [Magie profane](./profane.md)
