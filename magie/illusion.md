# Magie des illusions

Voilà la liste des illusions disponibles :

## Sorts d’illusion de niveau 0

**Signature magique** : inscrit la rune personnelle du lanceur (visible ou non).
**Détection des illusions** : détecte les illusions dans un rayon de 18 mètres pendant `1 minute / niveau` tant que la concentration est maintenue.
**Son imaginaire** : produit un son illusoire pendant `1 round / niveau`.
**Prestidigitation** : effectue un tour de passe-passe pendant `10 minutes / niveau`.
**Lecture de la magie** : permet de lire les grimoires et les parchemins pendant `10 minutes / niveau.` 
**Lumière dansante** : crée une torche ou une autre lumière pendant `1 minute / niveau`.

## Sorts d’illusion de niveau 1

**Hypnose** : fascine `2d4` DV de créatures pendant `2d4` rounds.
**Couleursdansantes** : assomme, aveugle et/ou étourdit `1d6` créatures dans un cône de 4,5 mètres.
**Déguisement** : modifie l’apparence du PJ pendant `10 minutes / niveau`.
**Image silencieuse** : crée une illusion visuelle mineure de votre choix.
**Ventriloquie** : permet de parler à distance pendant `1 minute / niveau`.

## Sorts d’illusion de niveau 2

**Invisibilité** : le lanceur est invisible pendant `1 minute / niveau` ou jusqu’au moment où il attaque. 
**Image imparfaite** : comme image silencieuse, mais avec quelques sons en plus.
**Image miroir** : crée des doubles illusoires du lanceur (`1d4 + 1 / 3 niveaux`, max. 8) pendant `1 minute / niveau`.
**Lueurs hypnotiques** : fascine (`2d4 + niveau`) DV de créatures pendant `bonus d’ESP + niveau du lanceur + 2` rounds.
**Hébètement** : la cible perd 1 action par tour pendant `1 round / niveau`.
**Flou** : le lanceur gagne +4 à la CA et ne peut pas être affecté par les attaques sournoises pendant `1 minute / niveau`.

## Sorts d’illusion de niveau 3

**Lumière du jour** : éclaire vivement dans un rayon de 18 mètres pendant `10 minutes / niveau`.
**Déplacement** : chaque attaque a 50% de chances de rater la cible pendant `1 round / niveau`.
**Image accomplie** : comme image silencieuse, plus sons, odeurs et température.
**Sphère d’invisibilité** : rend tout le monde invisible à3màlaronde.
**Sommeil profond** : endort 10 DV de créatures pendant `10 minutes / niveau`.
**Suggestion** : force la cible à accomplir un acte décidé par le lanceur pendant 1 heure/niveau ou jusqu’à ce que la tâche soit accomplie.

## Sorts d’illusion de niveau 4

**Charme-monstre** : un monstre croit être l’allié du lanceur pendant 1 jour/niveau.
**Confusion** : le sujet se comporte bizarrement pendant `1 round / niveau`.
**Assassin imaginaire** : illusion tuant la cible ou lui infligeant `3d6` points de dégâts.
**Invisibilité suprême** : comme invisibilité, mais continue quand le sujet attaque.
**Lueur d’arc-en-ciel** : fascine jusqu’à 24 DV de créatures pendant concentration + `1 round / niveau`. 
**Création mineure** : crée un objet en tissu ou bois.

## Sorts d’illusion de niveau 5

**Image prédéterminée** : comme image accomplie, mais sans concentration pendant `1 minute / niveau`. 
**Domination** : permet de contrôler un humanoïde par télépathie pendant 1 jour/niveau. 
**Annulation d’Enchantement** : libère la cible des Enchantements, des altérations, des malédictions et de la pétrification.
**Création majeure** : comme création mineure, plus pierre et métal.
**Faux-semblant** : modifie l’aspect d’une personne tous les 2 niveaux pendant 12 heures. 
**Songe** : envoie un message à un dormeur.

## Sorts d’illusion de niveau 6

**Vision lucide** : permet de voir la réalité telle qu’elle est pendant `1 minute / niveau`.
**Double illusoire** : rend le PJ invisible et crée son double illusoire.
**Image permanente** : comprend aspects visuels, sonores et olfactifs.
**Image programmée** : comme image accomplie, mais déclenché par condition.
**Traversée des ombres** : permet de voyager à une vitesse de 75 km/h pendant 1 heure/niveau.
**Suggestion de groupe** : comme suggestion, mais 1 cible/niveau.

## Sorts d’illusion de niveau 7

**Mot de pouvoir aveuglant** : aveugle jusqu’à 200 DV de créatures.
**Invisibilité de groupe** : comme invisibilité, mais multiples sujets.
**Projection d’image** : double illusoire pouvant parler et lancer des sorts pendant `1 round / niveau`. 
**Rayons prismatiques** : rayons magiques à effets variés dans un cône de 18 mètres.
**Porte de phase** : passage invisible au travers du bois ou de la pierre.
**Ennemi subconscient** : comme assassin imaginaire, mais à 9 mètres à la ronde.
