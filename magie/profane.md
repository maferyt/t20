# Magie profane

Voilà la liste des sorts profanes disponibles :

## Sorts profanes de niveau 0

**Signature magique** : inscrit la rune personnelle du PJ (visible ou non).
**Détection de la magie** : détecte les sorts et les objets magiques dans un rayon de 18 mètres pendant 1 minute/niveau et tant que le personnage maintient sa concentration.
**Son imaginaire** : produit un son illusoire pendant `1 round / niveau`.
**Lumière** : un objet brille comme une torche pendant `10 minutes / niveau`.
**Manipulation à distance** : permet de léviter jusqu’à 2,5 kg. S’annule si la concentration est perdue.
**Prestidigitation** : effectue un tour de passe-passe pour 1 heure.
**Lecture de la magie** : permet de lire les grimoires et les parchemins pendant `10 minutes / niveau`.

## Sorts profanes de niveau 1

**Feuille morte** : La créature ou l’objet tombe lentement pendant `1 round / niveau` ou jusqu’au sol. 
**Disque flottant** : disque flottant de 90 cm de diamètre pouvant porter jusqu’à 50 kg/niveau pendant 1 heure/niveau.
**Armure de mage** : donne au lanceur un bonus d’armure de +4 à la CA pendant 1 heure/niveau.
**Projectile magique** : inflige `1d4 + 1` dégâts, +1 projectile supplémentaire aux 2 niveaux après le premier (max. 5 projectiles).
**Sommeil** : endort magiquement jusqu’à 4 DV de créatures pendant 1 minute/niveau.
**Décharge électrique** : attaque de contact infligeant `1d6` points de dégâts d’électricité/niveau (max. `5d6`).
**Armure de mage** : donne au lanceur un bonus d’armure de +4 à la CA pendant 1 heure/niveau.
**Projectile magique** : inflige `1d4 + 1` dégâts, +1 projectile supplémentaire aux 2 niveaux après le premier (max. 5 projectiles).
**Sommeil** : endort magiquement jusqu’à 4 DV de créatures pendant 1 minute/niveau.
**Décharge électrique** : attaque de contact infligeant `1d6` points de dégâts d’électricité/niveau (max. `5d6`).

## Sorts profanes de niveau 2

**Flèche d’acide** : attaque de contact à distance infligeant 2d4 points de dégâts pendant 1 round + 1 round/3 niveaux.
**Sphère de feu** : crée une boule de feu qui roule au sol infligeant 2d6 dégâts pendant `1 round / niveau`. 
**Invisibilité** : le lanceur est invisible pendant 1 minute/niveau ou jusqu’au moment où il attaque. 
**Déblocage** : ouvre une porte, même si elle est scellée magiquement.
**Lévitation** : le lanceur s’élève dans les airs ou redescend à volonté pendant 1 minute/niveau.
**Pattes d’araignée** : permet de marcher sur les murs et les plafonds pendant `10 minutes / niveau`.

## Sorts profanes de niveau 3

**Clairaudience/Clairvoyance** :permet d’entendre/ de voir à distance pendant 1 minute/niveau.
**Dissipation de la magie** : annule les sorts et les effets magiques.
**Boule de feu** : inflige `1d6` points de dégâts de feu/ niveau (max. `10d6`) sur 6 mètres de rayon.
**Vol** : la cible vole à la vitesse de 18 mètres/round. 
**Éclair** : inflige `1d6` points de dégâts d’électricité/niveau (max. `10d6`).
**Baiser du vampire** : inflige `1d6` points de dégâts/2 niveaux (max. `10d6`), récupérés sous forme de pv par le lanceur.

## Sorts profanes de niveau 4

**Animation des morts** : Crée Niveau × 2 PV de squelettes ou de zombis.
**Œil du mage** : crée un œil invisible flottant qui avance de 9 mètres/round.
**Tentacules noirs** : tentacules immobilisant tout dans un rayon de 6 mètres pendant `1 round / niveau`.
**Porte dimensionnelle** : téléporte le lanceur sur une courte distance.
**Métamorphose** : change la forme du lanceur pendant 1 minute/niveau.
**Peau de pierre** : ignore 10 points de dégâts par attaque pendant `10 minutes / niveau` à concurrence de 10 points/niveau (max. 150).

## Sorts profanes de niveau 5

**Nuage mortel** : tue les créatures de 3 PV ou moins, 4 à 6 PV meurent en cas d’échec au jet de sauvegarde et celles de plus de 6 PV subissent un malus temporaire à la Force pendant 1 minute/niveau.
**Contact avec les plans** : permet de poser une question à une entité extérieure. S’annule si la concentration est perdue.
**Débilité** : l’Intelligence de la cible est réduite à 1. 
**Passe-muraille** : permet de traverser une surface en pierre ou en bois pendant 1 heure/niveau. 
**Permanence** : rend certains sorts permanents. 
**Téléportation** : téléporte le lanceur jusqu’à une distance de 150 km/niveau.

## Sorts profanes de niveau 6

**Champ d’anti-magie** : annule toute magie dans un rayon de 3 mètres pendant `10 minutes / niveau`. 
**Éclair multiple** : inflige `1d6` points de dégâts/niveau plus un éclair secondaire/niveau infligeant demi-dégâts (max. `20d6`).
**Prévoyance** : déclenche un autre sort selon une certaine condition. Perdure pendant `1 jour / niveau` ou jusqu’au déclenchement.
**Désintégration** : annihile une créature ou un objet. 
**Quête** : la créature effectue une tâche spécifique selon la volonté du lanceur. Dure `1 jour / niveau` ou jusqu’à la fin de la tâche.
**Vision lucide** : permet de voir la réalité telle qu’elle est pendant 1 minute/niveau.

## Sorts profanes de niveau 7

**Boule de feu à retardement** : inflige `1d6` points de dégâts de feu/niveau (max. `20d6`). L’effet peut-être retardé pendant 5 rounds.
**Forme éthérée** : Le lanceur devient éthéré pendant `1 round / niveau`.
**Doigt de mort** : tue la cible.
**Changement de plan** : permet jusqu’à 8 créatures de changer de plan.
**Mot de pouvoir aveuglant** : Aveugle les créatures ayant 200 pv ou mois pendant `1d4 + 1` minutes (permanent si la cible à moins de 50 pv). 
**Renvoi des sorts** : renvoie `1d4 + 6` niveaux de sort au lanceur pendant `10 minutes / niveau` ou jusqu’à utilisation.

## Sorts profanes de niveau 8

**Clone** : un double s’éveille à la mort de l’original. 
**Flétrissure** : inflige `1d6` points de dégâts/niveau à toutes les créatures dans un rayon de 9 mètres. 
**Nuage incendiaire** : nuage infligeant `4d6` points de dégâts de feu/round pendant `1 round / niveau`. 
**Danse irrésistible** : force la cible à danser pendant `1d4 + 1` rounds.
**Mot de pouvoir étourdissant** : étourdit une créature de 150 pv ou moins pendant `2d4 + 1` rounds. 
**Entrave** : enferme une créature dans une gemme.

## Sorts profanes de niveau 9

**Projection astrale** : emmène le lanceur et ses compagnons dans le plan Astral.
**Passage dans l’Éther** : permet au lancer de voyager dans le plan éthéré avec un compagnon pendant 1 minute/niveau.
**Portail** : relie deux plans pour voyager ou invoquer une entité. Dure `1 round / niveau`.
**Pluie de météores** : quatre sphères explosives infligent chacune `6d6` points de dégâts de feu.
**Mot de pouvoir mortel** : tue une créature possédant 100 PV ou moins.
**Capture d’âme** : capture une âme d’un défunt récent pour empêcher la résurrection.
