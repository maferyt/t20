# Armures

Voici la liste *non-exhaustive* des armures et boucliers disponibles. 

## Armures légères

| Armure               | Prix (PO) | CA             |
|----------------------|-----------|----------------|
| Matelassée           | 5         | 11 + Bonus DEX |
| Cuir                 | 10        | 11 + Bonus DEX |
| Cuir clouté          | 45        | 12 + Bonus DEX |

## Armures intermédiaires

| Armure               | Prix (PO) | CA                      |
|----------------------|-----------|-------------------------|
| Peau de bête         | 10 PO     | 12 + Bonus DEX (max +2) |
| Chemise de mailles   | 50 PO     | 13 + Bonus DEX (max +2) |
| Écailles             | 50 PO     | 14 + Bonus DEX (max +2) |
| Cuirasse             | 400 PO    | 14 + Bonus DEX (max +2) |
| Demi-plate           | 750 PO    | 15 + Bonus DEX (max +2) |


## Armures lourdes

| Armure               | Prix (PO) | Bonus |
|----------------------|-----------|-------|
| Broigne              | 30 PO     | 14    |
| Cotte                | 75 PO     | 16    |
| Clibanion            | 200 PO    | 17    |
| Harnois              | 1500 PO   | 18    |

## Boucliers

| Bouclier             | Prix (PO) | Bonus |
|----------------------|-----------|-------|
| Targe                | 15        | +1    |        
| Rondache en bois     | 5         | +1    |                 
| Rondache en acier    | 10        | +1    |                  
| Écu en bois          | 15        | +2    |              
| Écu en acier         | 20        | +2    |              
| Pavois               | 30        | +4    |        
