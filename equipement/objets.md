# Objets

Voici une liste *non-exhaustive* des différents objets pouvant être disponible dans des boutiques.

## Objets communs

| Article                          | Prix    |
|----------------------------------|---------|
| Aiguille à coudre                | 5 PA    |
| Balance de marchand              | 5 PO    |
| Billes (sac de 1000)             | 1 PO    |
| Bois de chauffage (pour un jour) | 1 PC    |
| Boite d'allume-feu               | 5 PA    |
| Bougie                           | 1 PC    |
| Boulier                          | 2 PO    |
| Bouteille en verre               | 2 PO    |
| Bélier portatif                  | 4 PO    |
| Cadenas                          | 10 PO   |
| Chausse-trappes (sac de 20)      | 1 PO    |
| Chaîne (3 m)                     | 5 PO    |
| Chevalière                       | 5 PO    |
| Chope en terre cuite             | 2 PC    |
| Cire à cacheter                  | 1 PO    |
| Cloche                           | 1 PO    |
| Coffre                           | 5 PO    |
| Corde en chanvre (15 m)          | 1 PO    |
| Corde en soie (15 m)             | 10 PO   |
| Couverture                       | 5 PA    |
| Couverture d'hiver               | 5 PA    |
| Craie (un morceau)               | 1 PC    |
| Cruche en terre cuite            | 2 PC    |
| Cruche ou pichet                 | 2 PC    |
| Échelle (3 m)                    | 1 PA    |
| Encre (bouteille de 30 ml)       | 10 PO   |
| Équipement d’escalade            | 25 PO   |
| Étui à cartes ou parchemins      | 1 PO    |
| Filet de pêche, 2,5m²            | 4 PO    |
| Fiole pour encre ou potion       | 1 PO    |
| Flasque (vide)                   | 3 PC    |
| Flasque ou chope (50 cl)         | 2 PC    |
| Gamelle                          | 2 PA    |
| Gourde (pleine)                  | 2 PA    |
| Grappin                          | 2 PO    |
| Grimoire                         | 50 PO   |
| Hameçon                          | 1 PA    |
| Huile, la flasque                | 1 PA    |
| Jarre en terre cuite             | 3 PC    |
| Lampe                            | 5 PA    |
| Lanterne sourde                  | 10 PO   |
| Lanterne à capote                | 5 PO    |
| Livre                            | 25 PO   |
| Longue-vue                       | 1000 PO |
| Loupe                            | 100 PO  |
| Marteau                          | 1 PO    |
| Marteau de forgeron              | 2 PO    |
| Masse                            | 1 PO    |
| Matériel de pêche                | 1 PO    |
| Menottes                         | 2 PO    |
| Miroir en acier                  | 5 PO    |
| Outre                            | 1 PO    |
| Paillasse                        | 1 PA    |
| Palan                            | 1 PO    |
| Panier (vide)                    | 4 PA    |
| Papier (une feuille)             | 2 PA    |
| Parchemin (une feuille)          | 1 PA    |
| Parfum (fiole)                   | 5 PO    |
| Pelle                            | 2 PO    |
| Perche (3 m)                     | 5 PC    |
| Petit miroir en acier            | 10 PO   |
| Picoeh de mineur                 | 3 PO    |
| Pied-de-biche                    | 2 PO    |
| Pierre à aiguiser                | 2 PC    |
| Pioche de mineur                 | 2 PO    |
| Piton                            | 5 PC    |
| Piège à mâchoires                | 5 PO    |
| Plume d'écriture                 | 1 PA    |
| Pointes en fer (10)              | 1 PO    |
| Pot en fer                       | 2 PO    |
| Rations de survie (par jour)     | 5 PA    |
| Robes                            | 1 PO    |
| Sablier                          | 25 PO   |
| Sac (vide)                       | 1 PA    |
| Sac de couchage                  | 1 PO    |
| Sac à dos (vide)                 | 2 PO    |
| Sacoche de ceinture              | 1 PO    |
| Savon                            | 1 PA    |
| Seau                             | 5 PC    |
| Sifflet                          | 5 PC    |
| Silex et amorce                  | 1 PO    |
| Tente                            | 2 PO    |
| Tonneau                          | 2 PO    |
| Torche                           | 1 PC    |

## Objets spéciaux

| Article                          | Prix    |
|----------------------------------|---------|
| Acide (fiole)                    | 25 PO   |
| Allume-feu                       | 1 PO    |
| Antidote (fiole)                 | 50 PO   |
| Bâton fumigène                   | 20 PO   |
| Bâton éclairant                  | 2 PO    |
| Eau bénite (flasque)             | 25 PO   |
| Feu grégeois (flasque)           | 20 PO   |
| Pierre à tonnerre                | 30 PO   |
| Potion de magie (`1d4 + 2` PM)   | 50 PO   |
| Potion de soins (`2d4 + 2` PV)   | 50 PO   |
| Poison (fiole)                   | 100 PO  |
| Sacoche immobilisante            | 50 PO   |
| Torche éternelle                 | 110 PO  |
| Trousse de soins                 | 5 PO    |

## Habillement

| Article                          | Prix    |
|----------------------------------|---------|
| Costume d'artiste                | 5 PO    |
| Vêtements communs                | 5 PA    |
| Vêtements fins                   | 15 PO   |
| Vêtements polaire                | 15 PO   |
| Vêtements voyage                 | 2 PO    |
