# Armes

Voici la liste *non-exhaustive* des armes disponibles. 

## Armes légères

| Arme                 | Dégats | Prix (PO) | Portée |
|----------------------|--------|-----------|--------|
| Dague                | 1d4    | 2 PO      | 6 m    |
| Gourdin              | 1d4    | 1 PA      |        |
| Hachette             | 1d6    | 5 PO      | 6 m    |
| Marteau léger        | 1d4    | 2 PO      | 6 m    |
| Serpe                | 1d4    | 1 PO      |        |
| Épée courte          | 1d6    | 10 PO     |        |
| Cimeterre            | 1d6    | 25 PO     |        |
| Masse d'armes légère | 1d6    | 5 PO      |        |

## Armes à une main

| Arme                 | Dégats | Prix (PO) | Portée |
|----------------------|--------|-----------|--------|
| Bâton                | 1d6    | 2 PA      |        |
| Fléau d'armes        | 1d8    | 10 PO     |        |
| Fouet                | 1d4    | 2 PO      |        |
| Hache d'armes        | 1d8    | 10 PO     |        |
| Javeline             | 1d6    | 5 PA      | 9 m    |
| Lance                | 1d6    | 1 PO      | 6 m    |
| Marteau de guerre    | 1d8    | 15 PO     |        |
| Masse d'armes        | 1d6    | 5 PO      |        |
| Morgenstern          | 1d8    | 15 PO     |        |
| Pic de guerre        | 1d8    | 5 PO      |        |
| Rapière              | 1d8    | 25 PO     |        |
| Trident              | 1d6    | 5 PO      | 6 m    |
| Épée longue          | 1d8    | 15 PO     |        |

## Armes à deux mains

| Arme                 | Dégats | Prix (PO) | Portée |
|----------------------|--------|-----------|--------|
| Coutille             | 1d10   | 20 PO     |        |
| Hache à deux mains   | 1d12   | 30 PO     |        |
| Hallebarde           | 1d10   | 20 PO     |        |
| Lance d’arçon        | 1d12   | 10 PO     |        |
| Maillet              | 2d6    | 10 PO     |        |
| Massue               | 1d8    | 2 PA      |        |
| Pique                | 1d10   | 5 PO      |        |
| Épée à deux mains    | 2d6    | 50 PO     |        |

## Armes à distance

| Arme                 | Dégats | Prix (PO) | Portée |
|----------------------|--------|-----------|--------|
| Arbalète de poing    | 1d6    | 75 PO     | 9 m    |
| Arbalète légère      | 1d8    | 25 PO     | 24 m   |
| Arbalète lourde      | 1d10   | 50 PO     | 30 m   |
| Arc court            | 1d6    | 25 PO     | 24 m   |
| Arc long             | 1d8    | 50 PO     | 45 m   |
| Fléchette (Dard)     | 1d4    | 5 PC      | 6 m    |
| Fronde               | 1d4    | 1 PA      | 9 m    |
| Filet                |        | 1 PO      | 1,50 m |
| Sarbacane            |        | 10 PO     | 7,50 m |

## Munitions et transport

| Munitions                     | Prix (PO) |
|-------------------------------|-----------|
| Aiguilles de sarbacane (50)   | 1 PO      |
| Carquois                      | 1 PO      |
| Étui à carreaux               | 1 PO      |
| Billes de fronde (20)         | 4 PC      |
| Carreaux d'arbalète (20)      | 1 PO      |
| Flèches (20)                  | 1 PO      |
