# Outils

Voici une liste *non-exhaustive* des différents outils pouvant être disponible dans des boutiques.

## Instruments de musique  

| Instrument                   | Prix    |
|------------------------------|---------|
| Chalemie                     | 2 PO    |
| Cor                          | 3 PO    |
| Cornemuse                    | 30 PO   |
| Flûte                        | 2 PO    |
| Flûte de pan                 | 12 PO   |
| Luth                         | 35 PO   |
| Lyre                         | 30 PO   |
| Tambour                      | 6 PO    |
| Tympanon                     | 25 PO   |
| Viole                        | 1030 PO |

## Jeux 

| Jeu                          | Prix    |
|------------------------------|---------|
| Dés                          | 1 PA    |
| Jeu d'échecs draconiques     | 1 PO    |
| Jeu de cartes                | 5 PA    |
| Jeu des Dragons              | 1 PO    |

## Outils d'artisan 

| Outil                        | Prix    |
|------------------------------|---------|
| Kit d'empoisonneur           | 50 PO   |
| Kit d'herboriste             | 5 PO    |
| Kit de contrefaçon           | 15 PO   |
| Kit de déguisement           | 25 PO   |
| Matériel d'alchimiste        | 50 PO   |
| Matériel de brasseur         | 20 PO   |
| Matériel de calligraphe      | 10 PO   |
| Matériel de peintre          | 10 PO   |
| Outils de bijoutier          | 25 PO   |
| Outils de bricoleur          | 50 PO   |
| Outils de cartographe        | 15 PO   |
| Outils de charpentier        | 8 PO    |
| Outils de cordonnier         | 5 PO    |
| Outils de forgeron           | 20 PO   |
| Outils de maçon              | 10 PO   |
| Outils de menuisier          | 1 PO    |
| Outils de potier             | 10 PO   |
| Outils de souffleur de verre | 30 PO   |
| Outils de tanneur            | 5 PO    |
| Outils de tisserand          | 1 PO    |
| Ustensiles de cuisinier      | 1 PO    |
| Outils de navigateur         | 25 PO   |
| Outils de voleur             | 25 PO   |
