# Classes

Voilà un descriptif plus complet des classes.

## ⚔️ Guerrier•ère

> J'ai un physique hors du commun. Je me bat avec une armure et un bouclier et je tape fort.

Particularités de la classe :

- vous pouvez porter n’importe quel type d’armure et utiliser un bouclier
- vous recevez un bonus de +3 au *Physique*
- vous ajoutez `+1` à tous vos jets d’attaque et de dégâts. Ces bonus augmentent de `+1` au niveau 5 et tous les cinq niveaux par la suite.

*Capacité spéciale* : Une fois par repos, vous pouvez bénéficier d'un *second souffle* et effectuer une action supplémentaire à votre tour.

## 🗡 Voleur•euse

> Je suis fourbe et rapide. Je me faufile et j'attaque mes ennemis sournoisement. 

Particularités de la classe :

- vous pouvez utiliser des armures légères
- vous avez un bonus de `+3` au *Subterfuge*
- vous pouvez attaquer au corps à corps avec votre bonus de *DEX* (si l'arme est légère)

*Capacité spéciale* : Une fois par tour, vous pouvez infliger des dégats supplémentaires à une créature que vous réussissez à toucher, à condition que cette créature soit concentrée sur autre chose que vous. Les dégats sont calculés de la manière suivante : `1d6 * (niveau du personnage / 2, arrondi à l'inférieur)`

## 🔥 Mage

> Je suis érudit•e. Je lance des sorts profanes, de la boule de feu à la lévitation, en passant par la manipulation d'objets à distance.

Particularités de la classe :

- vous ne portez aucune armure
- vous obtenez un bonus de `+3` en *Connaissance*
- vous pouvez lancer des [sorts profanes](../magie/sorts-profanes.md) (voir aussi : [Magie](../magie/magie.md)) 

*Capacité spéciale*: Vous pouvez apprendre d'autres sorts (non-divins). Tous les deux niveaux, vous pouvez choisir un sort de druide•sse ou d'illusionniste que vous ajoutez à votre liste de sorts connus. Ce sort doit respecter la [règle de niveau requis](./magie.md).

## 🩹 Prêtre•sse

> Je communique bien, avec mes alliés comme avec les dieux. Je peux lancer des sorts divins et repousser les mot vivants.

Particularités de la classe :

- vous pouvez porter une armure légère ou intermédiaire
- vous recevez un bonus de `+3` en *Communication*
- vous pouvez lancer des [sorts divins](./sorts/sorts-divins.md) (voir aussi : [Magie](./magie.md))

*Capacité spéciale* : Peut détruire les morts-vivants en réussissant une *Attaque Magique*. Pour réussir l'attaque magique, il faut que le résultat du jet soit supérieur aux PVs actuels du mort-vivant + 10. Le jet à effectuer est `1d20 + (niveau du lanceur * 2)`.

## 🙌 Paladin•e

> Je suis animé•e par la volonté de faire le bien sur cette terre, par la force ou par le soin, et je suis immunisé aux maladies.

Particularités de la classe :

- vous pouvez porter n’importe quel type d’armure et utiliser un bouclier
- vous obtenez `+1` en *Physique* et `+2` en *Communication*
- vous bénéficiez d’un bonus de `+1` à tous vos jets de sauvegarde. Ces bonus augmentent de `+1` au niveau 5 et tous les 5 niveaux par la suite. 
- vous pouvez, à volonté, détecter le mal sur un rayon de 18m

*Capacité spéciale* : Un•e paladin•e peut, grâce à une `imposition des mains`, guérir jusqu’à 5 pv par niveau et par jour. Au niveau 3, votre Divinité vous bénit et vous êtes maintenant immunisé•e aux maladies.

## 🏹 Rôdeur•euse

> Je sais survivre dans la nature et je suis imbattable lors qu'il s'agit de pister des créatures ou attaquer à distance.

Particularités de la classe :

- vous pouvez utilisez une armure (légère ou intermédiaire) et un bouclier
- vous bénéficiez d’un bonus de `+3` en *Survie*
- vous obtenez un bonus de `+1` à l'attaque et aux dégâts avec les armes à distance. Ces bonus augmentent de `+1` au niveau 5 et tous les cinq niveaux par la suite. 
- vous pouvez attaquer au corps à corps avec votre bonus de *DEX* (si l'arme est légère)

*Capacité spéciale* : Lorsque vous pistez d'autres créatures, vous découvrez aussi leur nombre exact, leurs tailles, et depuis combien de temps elles sont passées dans la zone. Et vous vous déplacez sans laisser de traces s'il vous le désirez.

## ✨ Illusionniste

> Je suis passé•e maître•sse dans l'art de la manipulation et des illusions. Je peux même me transformer ou disparaître.

Particularités de la classe :

- vous ne pouvez pas porter d’armure
- vous obtenez un bonus de `+2` en *Communication* et de `+1` en *Subterfuge*
- vous pouvez lancer des [sorts d’illusion](./sorts/sorts-d-illusion.md)

**Attention** : un personnage doit avoir une *DEX* minimum de 13 pour être illusionniste.

*Capacité spéciale* : Une fois par repos, vous pouvez créer une pièce magique. Vous faites apparaître une porte qui permet de vous cacher, où vous le souhaitez. Une fois à l'intérieur, la porte disparaît. La pièce est difficilement détectable et vous ne pouvez pas agir à l'extérieur lorsque vous êtes à l'intérieur. Cette pièce magique peut contenir un nombre de personne maximale = `niveau d'illusionniste / 2 arrondi à l'inférieur (minimum 1)`.

## ☘️ Druide•sse

> Je sais survivre dans la nature et je l'utilise pour arriver à mes fins. Je peux aussi me transformer en animal plusieurs fois par jour.

Particularités de la classe :

- vous pouvez porter toute armure ou bouclier qui ne soit pas en métal. 
- vous gagnez `+2` en Connaissance et `+2` en Survie
- vous êtes immunisé contre les effets « magiques » des créatures féeriques des bois.
- vous pouvez lancer des [sorts druidiques](./sorts/sorts-druidiques.md)

*Capacité spéciale* : Au niveau 3, un•e druide•sse peut se déplacer sans laisser de trace s’il•elle le désire. Au niveau 7, il•elle peut prendre la forme de n’importe quel animal de taille petite ou moyenne jusqu’à 3 fois par jour. En revenant à sa forme humaine, il•elle récupère 2 PVs/niveau.

## 🎶 Barde•sse

> Je chante/joue de la musique pour aider mes allié•e•s dans leurs prouesses. Je peux aussi rendre mes ennemis fous.

Particularités de la classe :

- vous pouvez porter une armure légère et utiliser une rondache
- vous bénéficiez d’un bonus de `+2` en *Communication*, *Subterfuge* et *Connaissance*

*Musique* : Vous pouvez chanter/jouer de la musique pendant autant de temps que vous le souhaitez. Cette musique aura des effets tant que vous chantez, puis pendant `niveau de barde / 2 arrondi à l'inférieur` tours. 

**Chants disponibles :**

- *Niveau 1* Contre-chant : contre les effets basés sur le son dans un rayon de 9m
- *Niveau 2* Chant talentueux : +1 aux jets de compétences
- *Niveau 4* Chant vaillant : +1 aux jets d'attaque et de dégâts
- *Niveau 6* Chant de charme : peut charmer une personne (3 fois par jour)
- *Niveau 8* Chant intrépide : +1 aux jets de sauvegarde et à la CA
- *Niveau 10* Chant strident : Vous devez réussir un jet de représentation (`1d20 + niveau de barde > 20`). Si vous réussissez la cible doit faire un jet d'*ESP* ou subir 2d6 dégats de folie.

*Capacité spéciale* : À partir du niveau 6, un•e barde•sse peut lancer des sorts comme un•e druide•sse ou un•e illusionniste (au choix) avec 5 niveaux en moins.
