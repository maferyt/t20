# Personnages pré-tirés

## 🗡 Morris, Voleur humain, niveau 1

FOR 12 (+1) - DEX 15 (+2) - ESP 12 (+1)
PV 18 - PM 1

Physique +2
Subterfuge +5
Connaissance +2
Communication +2
Survie +2

CA 15 : armure de cuir
Équipement : deux épées courtes, +0/+0, d6+1 ; 

## ⚔️ Kendric, Guerrier nain, niveau 1

FOR 16 (+3) - DEX 13 (+1) - ESP 11 (0)
PV 24 - PM 0

Physique +4
Subterfuge +1
Connaissance +1
Communication +1
Survie +1

CA 17 : cotte de mailles et bouclier
Équipement : épée longue, +5, d8+4

## 🔥 Cholmer, mage elfe, niveau 1

FOR 12 (+1) - DEX 13 (+1) - ESP 16 (+3)
PV 18 - PM 3

Physique +1
Subterfuge +1
Connaissance +4
Communication +1
Survie +1

CA 11 : Robe
Équipement : bâton, +2, d6+1
Sorts : tous les sorts profanes de niveau 0 et 1

## 🩹 Barnabas, prêtre halfelin, niveau 1

FOR 10 (0) - DEX 16 (+3) - ESP 13 (+1) 
PV 15 - PM 1

Physique +1
Subterfuge +1
Connaissance +1
Communication +4
Survie +1

CA 18 : Cotte de mailles
Équipement : Morgenstern, +1, d8

Sorts : tous les sorts divins de niveau 0 et 1
