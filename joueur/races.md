# Les races

Dans l'univers de **t20**, il y a 9 races différentes... Du moins, dans les races disponibles pour les joueur•euse•s. 

## Humain•e•s

Les *Humains* sont la plus jeune de toutes les races. Ils ne vivent pas très longtemps, contrairement aux autres, et se concentrent donc sur leur polyvalence.

Bonus de race : `+1` à toutes les compétences.

## Haut-Elfes

Les *Haut-elfes* sont des créatures magiques assez proches de la nature. Ils aiment l'art, la magie, ainsi que les bonnes choses de ce monde.

Bonus de race : `+2` en `ESP`.

## Nain•e•s

Un coup de marteau, deux coups de pioche, le tout dans un hall creusé à même la montagne, vous êtes au royaume des *nains*. Barbus, trapus, aimant boire et détestant les gobelins, ils sont très à cheval sur les traditions.

Bonus de race : `+2` en `FOR`.

## Halfelin•e•s

Agiles, casaniers et aimant le confort. C'est comme ça qu'on pourrait décrire les *halfelins*, ces personnes à la petite taille. Mais si certains sont posés, d'autres aiment l'aventure et la découverte de nouveaux horizons.

Bonus de race : `+2` en `DEX`.

## Gnomes

Un peu plus grands que les halfelin•e•s, les *gnomes* sont aussi plus explorateurs et curieux. Inventions, recherches ou créations, ils sont à l'origine de bien des choses.

Bonus de race : `+1` en `DEX` et `+1` en `ESP`.

## Demi-Orques

Certaines alliances improbables se forment et c'est ainsi que certaines races naissent. C'est le cas des *demi-orques* qui prennent plaisir à terroriser leurs voisins.

Bonus de race : `+4` en `FOR` et `-2` en `ESP`.

## Demi-Elfes

Alors que les demi-orques ne sont pas forcément les bienvenus, les *demi-elfes* sont largement plus acceptés en société. Ils combinent la polyvalence humaine avec l'amour de la nature et des arts des elfes. 

Bonus de race : `+1` en `DEX` et `+1` à deux compétences (au choix).

## Drakéides

Tout comme les demi-orques, les *drakéides* sont le fruit d'une alliance défendue et incomprise. Leur faible intellect est compensé par leur force et leur agilité.

Bonus de race : `+2` en `FOR`, `+2` en `DEX` et `-2` en `ESP`.

## Elfes Sylvains

Les *elfes sylvains* sont plus proches de la nature que leurs confrères "Haut-Elfes". Ils sont tout autant attirés par les arts et la magie, mais sont moins robustes que leurs cousins.

Bonus de race : `+2` en `DEX`, `+2` en `ESP` et `-2` en `FOR`.
